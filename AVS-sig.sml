signature INDEX =
  sig
    type int

    val same : int * int -> bool
  end

(* for complex numbers *)
signature INDEX2 =
  sig
    include INDEX

    val i0 : int
    val i1 : int
  end

signature ADDITIVE =
  sig
    type t

    val + : t * t -> t
    val - : t * t -> t
    val ~ : t -> t
    val zero : t
    val is_zero : t -> bool
  end

signature RING =
  sig
    include ADDITIVE

    val * : t * t -> t
  end

signature RING_1 =
  sig
    include RING

    (* the same for all *)
    val one : t
  end

signature RING_CONJUNCTION =
  sig
    include RING_1

    val conjugate : t -> t
    val abs2 : t -> t
    val tr : t -> t
  end

signature RING_DIV =
  sig
    include RING_1

    val / : t * t -> t
    val rev : t -> t
  end

signature RING_PARTIAL =
  sig
    include RING_DIV

    type r

    val fromRing : r -> t
    val   toRing : t -> r * r
  end

signature RING_PARTIAL_DM =
  sig
    include RING_PARTIAL

    val reduce : t -> t
    val entire : t -> r * t
  end

signature RING_DIVMOD =
  sig
    include RING_1

    val div : t * t -> t
    val mod : t * t -> t

    val gcd : t * t -> t
    val lcd : t * t -> t

    val gcdpq0 : t * t -> t * t
    val gcdpq1 : t * t -> t * t
    val gcdpq2 : t * t -> t * t
    val gcdpqs : t * t -> t
    (* Here GCD( m, a ) is supposed to be = 1 *)
    val lsolve : t * t * t -> t
    (* But here they can be arbitrary *)
    val linear : t * t * t -> ((t -> t) * t) option
  end

signature MODULO =
  sig
    include RING_1

    type r

    val   toRing : t -> r
    val fromRing : r -> t

    val modulo : r
  end

signature VECTOR_SPACE =
  sig
    include ADDITIVE

    type r

    val * : r * t -> t
  end

signature ADDITIVES_PRODUCT =
  sig
    include ADDITIVE

    structure I : INDEX

    type r

    val singleton : I.int * r -> t
    val foldli : (I.int * r * 'a -> 'a) -> 'a -> t -> 'a
    val foldri : (I.int * r * 'a -> 'a) -> 'a -> t -> 'a
  end

(* Arithmetic Vector Space *)
signature AVS =
  sig
    include ADDITIVES_PRODUCT

    val * : r * t -> t
  end

(* AVS over ring having 1 *)
signature AVS_1 =
  sig
    include AVS

    val map : (r -> r) -> t -> t
    val delta : I.int * I.int -> r
    val check : t -> I.int -> r
    val product : t -> r
    val comb  : (I.int -> t) -> t -> t
    val ort : I.int -> t
  end

(* AVS over ring having div\mod *)
signature AVS_DIVMOD =
  sig
    include AVS_1

    val gcsd : t -> r option
    val lcsd : t -> r option

    val reduce : t * r -> t
    val extract_scalar : t -> r option * t
    val system : t -> t -> r * r
  end

(* having Scalar Product *)
signature AVS_SP =
  sig
    include ADDITIVES_PRODUCT

    (* The type can match t then the operation
     * is internal multiplication of algebra
     * or it can match r then the operation
     * is scalar product
     *)
    type tt

    val * : t * t -> tt
  end
