functor Analytic_Group (
  structure G : GROUP
  structure I : INTEGER
) =
  struct
    type index = I.int
    datatype Range =
      Range of index * index

    val index0 = I.fromInt 0
    val index1 = I.fromInt 1

    fun inc i = I.+ (i, index1)
    fun dec i = I.- (i, index1)

		(*  f_i0 * ... * f_i1  *)
    fun analytic f (Range (i0, i1)) =
      let open I open G
      in case compare (i0, inc i1)
       of GREATER => analytic (inverse o f)
          (Range (inc i1, dec i0))
        | EQUAL => neutral
        | LESS => direct
          (f i1, analytic f
            (Range (i0, dec i1)) )
      end

		(*  x * ... * x  (n times)  *)
    fun of_constants x n =
      analytic (fn _ => x)
        (Range (index1, n))
  end

functor Analytic (
  structure R : RING_DIV
  structure I : INTEGER
) =
  struct
    structure AG =
      struct
        type t = R.t

        val direct   = R.+
        val inverse0 = R.-
        val inverse1 = R.-
        val inverse  = R.~
        val neutral  = R.zero
      end

    structure MG =
      struct
        type t = R.t

        val direct   = R.*
        val inverse0 = R./
        val inverse1 = R./
        val inverse  = R.rev
        val neutral  = R.one
      end

    structure A =
      Analytic_Group (
        structure G = AG
        structure I = I
      )

    structure M =
      Analytic_Group (
        structure G = MG
        structure I = I
      )

    val summa   = A.analytic
    val product = M.analytic

    val integer_scale = A.of_constants
    val integer_power = M.of_constants
    fun integer_scale_ x n =
      let open R
      in rev (integer_scale (rev x) n)
      end

    val index0 = I.fromInt 0
    and index1 = I.fromInt 1
    and index2 = I.fromInt 2
    and index3 = I.fromInt 3

    fun inc i = I.+ (i, index1)
    fun dec i = I.- (i, index1)
  end

functor Analytic_1 (
  structure R : RING_1
  structure I : INTEGER
) =
  struct
    structure QR =
      Ring_to_Partial (
        structure R = R
        val commuting = true
        (*
         * Until only operations defined in R performed
         * the result stays in R (denominator stays = 1)
         * but multiplication for 1 is always commuting
         *)
      )
    structure AQR =
      Analytic (
        structure R = QR
        structure I = I
      )
    fun injection f x i =
      #1 (QR.toRing (f (QR.fromRing x) i))
    val iscale = injection AQR.integer_scale
    val ipower = injection AQR.integer_power
    (* Power scale must be non-negative *)
  end

functor Analytic_DivMod (
  structure R : RING_DIVMOD
  structure I : INTEGER
) =
  struct
    structure QR =
      Ring_DivMod_to_Partial (
        structure R = R
        val commuting = true
      )
    structure AQR =
      Analytic (
        structure R = QR
        structure I = I
      )
    fun injection f x i =
      #1 (QR.entire (f (QR.fromRing x) i))
    val iscale  = injection AQR.integer_scale
    val iscale_ = injection AQR.integer_scale_
    val ipower  = injection AQR.integer_power
  end

functor Index_Analytic (
  structure I : INTEGER
) =
  struct
    structure R =
      Integer_to_Ring_DivMod ( I )
    structure P =
      Ring_DivMod_to_Partial (
        structure R = R
        val commuting = true
      )
    structure A =
      Analytic (
        structure R = P
        structure I = I
      )
    open A

    val id = (fn x => x)
    val entire = #1 o P.entire
    val fromIndex =
      integer_scale P.one

    fun tabulate (n, f) =
      List.tabulate (I.toInt n, f o I.fromInt)

    fun factorialk k n =
      let open I
        val d = n div k
        and m = n mod k
        val start = if (m = index0)
          then index1 else index0
        fun lin x = P.+ (fromIndex m,
          P.* (fromIndex k, fromIndex x))
      in entire (M.analytic lin (M.Range (start, d)))
      end
    fun factorialsk k n =
      tabulate (inc n, factorialk k)
    val factorial  = factorialk index1
    val factorial2 = factorialk index2

    fun binomial n i =
      let open P
        val c = (fn k => ((fromIndex n) - (k - one)) / k) o fromIndex
      in entire (M.analytic c (M.Range (index1, i)))
      end
    fun binomials n =
      tabulate (inc n, binomial n)

    (* k is count of non-zero indices, n is summa of them *)
    fun count_of_monomials k n =
      if (k = index1) then P.one else
        let fun com i =
          count_of_monomials (dec k) i
        in A.analytic com (A.Range (index0, n))
        end

    (* It consumes pretty long time for just n = 256 *)
    fun count_of_monomials_test n =
      let fun an n =
        let open I in (n + index1) *
          (n + index2) div index2
        end
        open P
      in List.all id (tabulate
        (inc n, (fn n => is_zero (
          (count_of_monomials index3 n) -
            (fromIndex (an n))
        )))
      )
      end
  end
