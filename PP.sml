(*
 * Pretty Printing of a Polynomial
 *)
functor PPP (
  X : sig
    structure S : AVS
    structure I : INTEGER
    val show_ring : S.r -> string
    sharing type S.I.int = I.int
  end
) =
  struct
    open X.S
  
    val show_int = X.I.toString
    fun show_monom show (i, x) =
      let
        val z = (i = (X.I.fromInt 0))
        and e = (i = (X.I.fromInt 1))
      in (show x) ^
      (if z then "" else " x" ^
        (if e then "" else "^" ^ (show_int i)
        )
      )
      end
    fun show_poly show x =
      let
        fun f (i, x, NONE  ) = SOME              (show_monom show (i, x))
          | f (i, x, SOME y) = SOME (y ^ " + " ^ (show_monom show (i, x)))
      in case (foldli f NONE x)
       of NONE => "0"
        | SOME s => s
      end
    fun show_list nil = "[]"
      | show_list (x :: xx) =
      let
        fun show_rest nil = ""
          | show_rest (x :: xx) =
            (", " ^ x ^ (show_rest xx))
      in "[ " ^ x ^ (show_rest xx) ^ " ]"
      end
    fun print_line x = print (x ^ "\n")
    fun show_poly_list show =
      print_line o show_list o (List.map (show_poly show))
    val print_poly_list = show_poly_list X.show_ring
  end
