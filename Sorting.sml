structure Vector_Sorting =
  struct


  (* Merge sorting *)

  fun middle ( k0, k1 ) = ( k0 + k1 ) div 2

  fun copy_subvector ( v0, v1 ) ( k0, k1, l ) =
    if k0 = k1 then v1 else
      let val x0 = Vector.sub ( v0, k0 )
          val v1 = Vector.update ( v1, l, x0 )
      in copy_subvector ( v0, v1 ) ( k0 + 1, k1, l + 1 ) end

  fun merge_subvectors r v0 ( k0, k, k1 ) =
    let fun f ( v1, i0, i1, i ) =
      if i0 = k then copy_subvector ( v0, v1 ) ( i1, k1, i ) else
      if i1 = k1 then copy_subvector ( v0, v1 ) ( i0, k, i ) else
      let val x0 = Vector.sub ( v0, i0 )
          and x1 = Vector.sub ( v0, i1 )
          and i' = i + 1
      in if r ( x0, x1 )
        then f ( Vector.update ( v1, i, x0 ), i0 + 1, i1, i' )
        else f ( Vector.update ( v1, i, x1 ), i0, i1 + 1, i' ) end
    in f ( v0, k0, k, k0 ) end

  fun merge_sort_subvector r v ( k0, k1 ) =
    if k0 + 1 = k1 then v else
      let val k = middle ( k0, k1 )
          val v = merge_sort_subvector r v ( k0, k )
          val v = merge_sort_subvector r v ( k, k1 )
      in merge_subvectors r v ( k0, k, k1 ) end

  fun merge_sort_vector r v =
    merge_sort_subvector r v ( 0, Vector.length v )


  (* Quick sorting *)

  fun swap v0 ( k0, k1 ) =
    let val x0 = Vector.sub ( v0, k0 )
        and x1 = Vector.sub ( v0, k1 )
        val v1 = Vector.update ( v0, k0, x1 )
        val v2 = Vector.update ( v1, k1, x0 )
    in v2 end

  fun inc i = i + 1
  fun dec i = i - 1
  fun incii ( i0, i1 ) = ( inc i0, i1 )
  fun decii ( i0, i1 ) = ( i0, dec i1 )
  fun incdecii ii = incii ( decii ii )
  fun eqii ( i0, i1 ) = i0 = i1

  (* первый элемент, для которого предикат выполняется *)
  fun find_up p v ( ii as ( i0, i1 ) ) =
    if eqii ii then i1 else
      if p ( Vector.sub ( v, i0 ) )
        then i0 else find_up p v ( incii ii )
  (* элемент, следующий за последним, для которого предикат не выполняется *)
  fun find_down p v ( ii as ( i0, i1 ) ) =
    if eqii ii then i0 else
      if p ( Vector.sub ( v, dec i1 ) )
        then find_down p v ( decii ii ) else i1

  fun split_subvector p v0 ( ii as ( i0, i1 ) ) =
    if eqii ii then ( i0, v0 ) else
      let val k0 = find_up   p v0 ii
          and k1 = find_down p v0 ii
          val kk = ( k0, k1 )
      in    (* 1 <> k1 - k0 *)
        if eqii kk then ( k0, v0 ) else
          let val v1 = swap v0 ( decii kk ) in
          split_subvector p v1 ( incdecii kk ) end
      end

  fun quick_sort_subvector r v0 ( kk as ( k0, k1 ) ) =
    if eqii kk then v0 else
      let val x0 = Vector.sub ( v0, k0 )
          val p = fn x => r ( x0, x )
          val ( k, v ) = split_subvector p v0 kk
      in  if k0 = k then quick_sort_subvector r v ( incii kk ) else
          (* для строгих неравенств r *)
          if k = k1 then let val v' = swap v ( decii kk ) in
            quick_sort_subvector r v' ( decii kk ) end else
          (* для не строгих *)
          let val v = quick_sort_subvector r v ( k0, k )
              val v = quick_sort_subvector r v ( k, k1 ) in v end
      end

  fun quick_sort_vector r v =
    quick_sort_subvector r v ( 0, Vector.length v )


  (* Simple sorting *)

  fun extremal_of_subvector r v ( ii as ( i0, _ ) ) =
    let fun f ( im, m ) ( ii as ( i0, _ ) ) =
      if eqii ii then im else
        let val x = Vector.sub ( v, i0 )
        in if r ( x, m )
          then f ( i0, x ) ( incii ii )
          else f ( im, m ) ( incii ii ) end
    in f ( i0, Vector.sub ( v, i0 ) ) ( incii ii ) end
  fun simple_sort_subvector r v ( ii as ( i0, _ ) ) =
    if eqii ii then v else let val v1 = swap v
        ( i0, extremal_of_subvector r v ii )
      in simple_sort_subvector r v1 ( incii ii ) end
  fun simple_sort_vector r v =
    simple_sort_subvector r v ( 0, Vector.length v )

  end


  (* Testing *)

  fun is_vector_ordered r v = let val f =
      fn ( x, NONE           ) => SOME ( x, true )
       | ( x, SOME ( x0, s ) ) => SOME (
        if s then x else x0, s andalso r ( x0, x ) )
    in Vector.foldl f NONE v end
  fun is_list_ordered r l = let val f =
      fn ( x, NONE           ) => SOME ( x, true )
       | ( x, SOME ( x0, s ) ) => SOME (
        if s then x else x0, s andalso r ( x0, x ) )
    in List.foldl f NONE l end

  fun test_sorting_function sf r v =
    is_vector_ordered r ( sf r v )

  fun test_simple_sorting r v =
    test_sorting_function Vector_Sorting.simple_sort_vector r v
  fun test_quick_sorting r v =
    test_sorting_function Vector_Sorting.quick_sort_vector r v
  fun test_merge_sorting r v =
    test_sorting_function Vector_Sorting.merge_sort_vector r v

  fun test_all_sortings r v = [
    test_simple_sorting r v,
    test_quick_sorting r v,
    test_merge_sorting r v ]
  fun is_test_all_sortings_passed r v =
    is_list_ordered ( op = ) ( test_all_sortings r v )
  fun test r l = map ( is_test_all_sortings_passed r ) l

;
  test ( op <= ) [
    #[0,1,2,3,4,5,6,7,8,9] ,
    #[9,8,7,6,5,4,3,2,1,0] ,
    #[3,5,7,2,4,6,8,1,0,9] ]
;
  test ( op >= ) [
    #[0,1,2,3,4,5,6,7,8,9] ,
    #[9,8,7,6,5,4,3,2,1,0] ,
    #[3,5,7,2,4,6,8,1,0,9] ]
;

(*
use "Sorting.sml" ;
*)
