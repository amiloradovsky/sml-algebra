functor Conjunction (
  X : sig
    structure R : RING_1
    val conjugate : R.t -> R.t
  end
) : RING_CONJUNCTION =
  struct
    open X.R

    val conjugate = X.conjugate
    fun abs2 x = x * (conjugate x)
    fun tr   x = x + (conjugate x)
  end

structure Index2 :> INDEX2 =
  struct
    datatype int = i0 | i1

    fun same (i0, i0) = true
      | same (i1, i1) = true
      | same _ = false
  end

functor Complex ( R : RING_1 ) =
  struct
    local
      structure S0 =
        Ring_to_Finite_AVS (
          structure R = R
          structure I = Index2
        )
      structure S =
        Build_AVS_1 (
          structure R = R
          structure S = S0
        )

      val i0 = Index2.i0
      val i1 = Index2.i1

      val z = S.zero
      val e = S.ort i0
      val i = S.ort i1

      val ieq2 = S.I.same

      fun ieq3 (n, m, i) =
        (ieq2 (n, i)) andalso
        (ieq2 (m, i))

      fun gen_table ii (n, m) =
        if ieq3 (n, m, i0) then e  else
        if ieq3 (n, m, i1) then ii else i

      functor Complex_ (
        val ii : S.t
      ) : RING_CONJUNCTION =
        Conjunction (
          structure R =
            AVS_to_Algebra_1 (
              structure S = S
              val one = e
              val table = gen_table ii
            )
          fun conjugate x =
            let open S
              val re = check x i0
              and im = check x i1
            in re * e - im * i
            end
        )

      functor Complex (
        val ii : S.t
      ) =
        struct
          local
            structure C =
              Complex_ ( val ii = ii )
          in
            val z = z
            val e = e
            val i = i
            fun re x = S.check x i0
            fun im x = S.check x i1

            structure S = S0

            open C
          end
        end
    in
      structure Hyperbolic = Complex ( val ii = S.~ e )
      structure Parabolic  = Complex ( val ii =     z )
      structure Elliptic   = Complex ( val ii =     e )
    end (* local *)
  end (* struct *)

functor Euclidean_Space (
  X : sig
    structure R : RING_1
    structure I : INDEX2
  end
) =
  struct
    structure S0 =
      Ring_to_Finite_AVS ( X )
    structure S =
      Build_AVS_1 (
        structure R = X.R
        structure S = S0
      )
    structure E =
      Euclidean_AVS (
        structure R = X.R
        structure S = S
      )
  end
