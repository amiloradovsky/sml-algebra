use "Monoid-sig.sml" ;
use "AVS-sig.sml" ;
use "Ring.sml" ;
use "Dynamic.sml" ;
use "AVS.sml" ;
use "Partial.sml" ;
use "Complex.sml" ;
use "Analytic.sml" ;
use "Polynomial.sml" ;
use "Modulo.sml" ;
use "PP.sml" ;
use "Integer.sml" ;

(*
/s/smlnj_/bin/sml
*)
