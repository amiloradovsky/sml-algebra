
(* TODO: do not require whole ADDITIVE for values,
 * just zero and test for zero
 *)

signature DYNAMIC =
  sig
    type table
    type key
    type value

    val empty : table
    val is_empty : table -> bool
    val assert : key * value * table -> table
    val check  : key * table -> value
    val map : (value -> value) -> table -> table
    val foldli : (key * value * 'b -> 'b) -> 'b -> table -> 'b
    val foldri : (key * value * 'b -> 'b) -> 'b -> table -> 'b
  end

functor Dynamic_over_Additive (
  structure R : ADDITIVE
  structure I : INDEX
) :> DYNAMIC where type value = R.t
               and type key = I.int =
  struct
    type value = R.t
    type key = I.int
    type node = key * value
    type table = node list

    val empty = nil
    val is_empty = List.null

    fun check (i, nil) = R.zero
      | check (i, (j, x) :: aa) =
        if I.same (i, j) then x else check (i, aa)

    fun assert (i, x, nil) =
        if R.is_zero x then nil
          else (i, x) :: nil
      | assert (i, x, (iy as (j, y)) :: aa) =
        if I.same (i, j)
          then ( if R.is_zero x then aa else ( i, x ) :: aa )
          else ( iy :: ( assert ( i, x, aa ) ) )

    fun foldli f y ((i, x) :: aa) =
          foldli f (f (i, x, y)) aa
      | foldli _ y nil = y
    fun foldri f y ((i, x) :: aa) =
          f (i, x, (foldli f y aa))
      | foldri _ y nil = y
    fun map f ((i, x) :: aa) =
        let val y = f x
        in if R.is_zero y
          then map f aa
          else (i, y) :: (map f aa)
        end
      | map _ nil = nil
  end

(* a "database" containing just two elements, used for complex numbers *)
functor Static_over_Additive (
  structure R : ADDITIVE
  structure I : INDEX2
) :> DYNAMIC where type value = R.t
               and type key = I.int =
  struct
    type 'a pair = 'a * 'a

    type value = R.t
    type key = I.int
    type table = value pair

    val empty = (R.zero, R.zero)
    fun is_empty (a, b) =
      R.is_zero a andalso
      R.is_zero b
    fun assert (i, a, (x, y)) =
      if I.same (i, I.i0)
        then (a, y) else (x, a)
    fun check (i, (x, y)) =
      if I.same (i, I.i0)
        then x else y
    fun map f (x, y) = (f x, f y)

    fun foldli f a (x, y) =
      f (I.i1, y, f (I.i0, x, a))
    fun foldri f a (x, y) =
      f (I.i0, x, f (I.i1, y, a))
  end
