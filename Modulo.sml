functor Modulo (
  X : sig
		structure R : RING_DIVMOD
    val modulo : R.t
  end
) :> MODULO where type r = X.R.t =
  struct
    open X.R
		
		type r = t

    val modulo = X.modulo

    fun fromRing x = x mod modulo
    fun   toRing x = x

    val op ~ = (fn x => modulo - x)

    local
      fun f oper xy = ( oper xy ) mod modulo
    in
      val op + = f op +
      val op - = f op -
      val op * = f op *
    end
  end
