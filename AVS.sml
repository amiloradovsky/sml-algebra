functor General_AVS (
  structure R : RING
  structure I : INDEX
  structure Dyn : DYNAMIC
  sharing type Dyn.value = R.t
  sharing type Dyn.key = I.int
) =
  struct
    structure I = I

    type t = Dyn.table
    type r = R.t

    fun add sum (i, x, a) =
      Dyn.assert (i, sum (Dyn.check (i, a), x), a)
    fun op + (a, b) = Dyn.foldli (add R.+) a b
    fun op - (a, b) = Dyn.foldli (add R.-) a b
    val zero = Dyn.empty
    val is_zero = Dyn.is_empty
    fun op ~ a = zero - a

    fun op * (a, x) =
      Dyn.map (fn b => R.* (a, b)) x

    fun singleton (i, x) =
      Dyn.assert (i, x, zero)
    val foldli = Dyn.foldli
    val foldri = Dyn.foldri
  end

(* two dimensional vector space, used for complex extensions *)
functor Ring_to_Finite_AVS (
  X : sig
    structure R : RING
    structure I : INDEX2
  end
) :> AVS where type I.int = X.I.int
           and type r = X.R.t =
  struct
    structure Dyn =
      Static_over_Additive (
        structure R = X.R
        structure I = X.I
      )
    structure S =
      General_AVS (
        structure R = X.R
        structure I = X.I
        structure Dyn = Dyn
      )
    open S
  end

functor Ring_to_Universal_AVS (
  X : sig
    structure R : RING
    structure I : INDEX
  end
) :> AVS where type I.int = X.I.int
           and type r = X.R.t =
  struct
    structure Dyn =
      Dynamic_over_Additive (
        structure R = X.R
        structure I = X.I
      )
    structure S =
      General_AVS (
        structure R = X.R
        structure I = X.I
        structure Dyn = Dyn
      )
    open S
  end

functor Extend_Additive (
    structure S : ADDITIVE
    structure S0 : ADDITIVES_PRODUCT
) =
  struct
    open S

    fun sumi f =
      let fun add f (i, x, y) =
          f (i, x) + y
      in S0.foldli (add f) zero
      end
  end

functor Extend_AVS (
  X : sig
    structure S  : VECTOR_SPACE
    structure S0 : ADDITIVES_PRODUCT
    sharing type S0.r = S.r
  end
) =
  struct
    local
      structure A =
        Extend_Additive ( X )
    in open A open X.S
      fun comb f = sumi
        (fn (i, x) => x * (f i))
    end
  end

functor AVS_Product (
  X : sig
    structure S : VECTOR_SPACE
    structure S0 : AVS
    sharing type S0.r = S.r
    val table : S0.I.int * S0.I.int -> S.t
  end
) : AVS_SP =
  struct
    open X.S0

    type tt = X.S.t

    local open X.S
      structure ES =
        Extend_AVS ( X )
    in val op * =
      (fn (x, y) =>
        let fun fy j =
          let fun fx i =
            X.table (i, j)
          in ES.comb fx x
          end
        in ES.comb fy y
        end
      )
    end
  end

functor AVS_to_Algebra (
    structure S : AVS
    val table : S.I.int * S.I.int -> S.t
) : RING = AVS_Product (
  struct
    structure S  = S
    structure S0 = S
    val table = table
  end
)

(* explicit setting of one *)
functor AVS_to_Algebra_1 (
    structure S : AVS
    val one : S.t
    val table : S.I.int * S.I.int -> S.t
) : RING_1 =
  struct
    local
      structure R = AVS_Product (
        structure S  = S
        structure S0 = S
        val table = table
      )
    in open R
      val one = one
    end
  end

functor Ring_as_Vector_Space ( R : RING ) : VECTOR_SPACE =
  struct
    open R
    type r = t
  end

functor Build_AVS_SP (
  X : sig
    structure R : RING
    structure S : AVS
    sharing type S.r = R.t
    val table : S.I.int * S.I.int -> R.t
  end
) : AVS_SP =
  AVS_Product (
    open X

    structure S0 = S
    structure S =
      Ring_as_Vector_Space ( R )
    val table = table
  )

functor Euclidean_AVS (
  X : sig
    structure R : RING_1
    structure S : AVS_1
    sharing type S.r = R.t
  end
) =
  Build_AVS_SP (
    open X

    val table = S.delta
  )

functor Build_AVS_1 (
    structure R : RING_1
    structure S : AVS
    sharing type S.r = R.t
) : AVS_1 =
  struct
    structure ER =
      Extend_AVS (
        structure S0 = S
        structure S =
          Ring_as_Vector_Space ( R )
      )
    structure ES =
      Extend_AVS (
        structure S  = S
        structure S0 = S
      )

    fun delta (i, k) =
      if S.I.same ( i, k ) then R.one else R.zero

    fun check x i =
      let fun delta_x k =
        delta (i, k)
      in ER.comb delta_x x
      end

    val product =
      let open R
      in S.foldli
        (fn (_, x, y) => x * y)
        one
      end

    val comb = ES.comb

    open S

    fun ort i =
      singleton (i, R.one)

    fun map f = ES.sumi
      (fn (i, x) => (f x) * (ort i))
  end

functor Extend_AVS_DivMod (
  X : sig
    structure R : RING_DIVMOD
    structure S : AVS_1
    sharing type S.r = R.t
  end
) : AVS_DIVMOD =
  struct
    structure E = Euclidean_AVS ( X )
    (*
     * Greatest Common Scalar Divider which deg is > 1
     * returns NONE only if parameter is zero
     *)
    val gcsd =
      let
        fun gcd (_, x, NONE  ) = SOME x
          | gcd (_, x, SOME y) = SOME (X.R.gcd (x, y))
      in X.S.foldli gcd NONE
      end
    fun lcsd x =
      let val gcd = gcsd x
      in case gcd
       of SOME g => SOME (X.R.div (X.S.product x, g))
        | NONE   => NONE
      end
    fun reduce (p, d) =
      X.S.map (fn x => X.R.div (x, d)) p
    fun extract_scalar x =
      let val d = gcsd x
      in case d
       of NONE   => (d, x)
        | SOME a => (d, reduce (x, a))
      end

    (* mm is sipposed to be pairwise prime *)
    fun system mm bb =
      let open X.R
        val M = X.S.product mm
        fun coef m =
          let
            val M  = M div m
            val M' = lsolve (M, one, m)
          in M * M'
          end
        val aa = X.S.map coef mm
      in ((E.* (aa, bb)) mod M, M)
      end

    open X.S
  end
