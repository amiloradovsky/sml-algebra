
  signature Equation =
    sig

      include VECTOR_SPACE

    end

  signature System =
    sig

      type t  (* element *)
      type s  (* system of them *)
      (* in particular it may be a set *)

      val map : ( t -> t ) -> s -> s
      val apply : ( t * t -> t ) -> s -> s
      (* Sequently process the system.
       * Each step choose another element and then
       * process rest of the system using the operation and it.
       * An element derived from once already chosen one may not be chosen yet one time.
       *)

    end

  signature Countable_System =
    sig

      include System

      val empty : s

      val add : t * s -> s
      val any : s -> ( t * s ) option

    end

  functor Linear_System (
    X : sig
      structure R : Ring
      structure E : AVS
(* TODO: AVS has operation `any' of choosing a non-zero index according to predicat *)
      structure S : System
      sharing type R.t = E.r
      sharing type S.t = E.t
    end
  ) =
    struct

      (* if division with remainder is defined
       * then the exclusion may be done more efficiently
       *)

      fun exclude i ( e0, e1 ) =
        let open X.E in ( extract i e1 ) * e0 -
                        ( extract i e0 ) * e1 end

      fun select_and_exclude anyi ( ( _, e1 ) as ee ) =
        exclude ( anyi e1 ) ee

      fun solve p s = apply ( select_and_exclude ( any p ) ) s

      (* 'exclude' is an operation of exclusion of the variable from an equation
       * 'p' is a predicate of choosing an index to exclude
       * (the coefficent isn't equal to zero in the equation)
       * Carries the system to a form where each index of given set
       * has just one equation having corresponding coefficent not equal to zero.
       * The index corresponding to the equation may be got trough 'anyi' operation.
       *)

    end

