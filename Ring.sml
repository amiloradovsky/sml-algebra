functor Integer_to_Ring_1 ( I : INTEGER ) : RING_1 =
struct
    open I
    type t = int

    val zero = I.fromInt 0
    val one  = I.fromInt 1
    fun is_zero x =
      case (I.compare (x, zero))
       of EQUAL => true
        | _     => false
end

functor Extend_Ring_DivMod (
  R : sig
    include RING_1

    val div : t * t -> t
    val mod : t * t -> t
  end
) : RING_DIVMOD =
  struct
    open R

    fun gcdpq_ (xy as (x, y),
        pq2 as (p2, q2),
        pq1 as (p1, q1), s) =
      let
        val d = op div xy
        and m = op mod xy
        val pq = (
          p2 + d * p1,
          q2 + d * q1)
      in if (is_zero m)
        then ( y, pq, pq1, pq2, s )
        else gcdpq_ ( (y, m), pq1, pq, ~s )
      end
    fun gcdpq xy = gcdpq_ (xy,
      (zero, one), (one, zero), one)

    val gcd = #1 o gcdpq
    fun lcd (x, y) =
      (x * y) div (gcd (x, y))

    val gcdpq0 = #2 o gcdpq  (* for testing *)
    val gcdpq1 = #3 o gcdpq
    val gcdpq2 = #4 o gcdpq
    val gcdpqs = #5 o gcdpq  (* parity *)

    (* find x satisfying equation
     *   a x = b (mod m)
     *)
    fun lsolve (a, b, m) =
      let
        val (p, _) = gcdpq1 (m, a)
        and  s     = gcdpqs (m, a)
      in (s * p * b) mod m
      end

    fun linear (a0, b0, m0) =
      let
        val d = gcd (a0, m0)
        val has =  (* a solution *)
          is_zero (b0 mod d)
        fun x t =
          let
            val b = b0 div d
            and a = a0 div d
            and m = m0 div d
          in t * m + lsolve (a, b, m)
          end
      in if has
        then SOME (x, d)
        else NONE
      end

    (* comb : x #1 = y #2 = lcd *)
    fun comb (xy as (x, y)) =
      let val d = gcd xy
      in (y div d, x div d)
      end
  end

functor Integer_to_Ring_DivMod (
  I : INTEGER
) : RING_DIVMOD =
  struct
    structure R1 =
      Integer_to_Ring_1 ( I )
    structure RDM =
      Extend_Ring_DivMod (
        open I
        open R1
      )
    open RDM
  end

(* If the ring is not (anti)commutative,
 * it can be used to apply an "one sided" functor
 * (such as building AVS over Ring) in "reverse order"
 * giving thus algebraic structure with different
 * properties built on the same ring
 *)
functor Swap_Ring ( R : RING ) : RING =
  struct
    open R

    local fun swap (a, b) = (b, a)
    in val op * = op * o swap
    end
  end

functor Make_Commuting (
    structure R : RING
    val commuting : bool
) : RING =
  struct
    open R

    val op * = fn (a, b) =>
      (if commuting
        then op + else op -
      ) (a * b, b * a)
  end
