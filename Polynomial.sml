(* Base ring supposed to be commuting *)
functor Polynomial_1 (
  X : sig
    structure S : AVS_1
    structure I : INTEGER
    val dimension : I.int option
    sharing type S.I.int = I.int
  end
) : RING_1 =
  AVS_to_Algebra_1 (
    local open X.I in

    structure S = X.S
    val one = S.ort (fromInt 0)
    fun table (i, j) = S.ort
      let val k = i + j
      in case X.dimension
       of NONE   => k
        | SOME d => if k < d
          then k else k - d
      end

    end
  )

functor Extend_Polynomial_1 (
  X : sig
    structure R : RING_1
    structure S : AVS_1
    structure I : INTEGER
    val dimension : I.int option
    sharing type S.I.int = I.int
    sharing type S.r = R.t
  end
) =
  struct
    local
      structure R =
        Polynomial_1 ( X )
    in
      local open X.S in
        val index0 = X.I.fromInt 0
        val index1 = X.I.fromInt 1
        val monomial = ort
        fun scalar x =
          x * monomial index0
        val symbol = monomial index1
      end

      val ranges =
        let fun maxmin (i, _, mm) =
          case mm
           of NONE            => SOME (i, i)
            | SOME (min, max) => SOME (
              X.I.min (min, i),
              X.I.max (max, i))
        in X.S.foldli maxmin NONE
        end

      fun extract x =
        let
          fun reduce (x, d) =
            let fun f i =
              monomial (X.I.- (i, d))
            in X.S.comb f x
            end
        in case (ranges x)
         of NONE => (NONE, x)
          | SOME (a, b) =>
            let val d = X.I.- (b, a)
            in (SOME (monomial d), reduce (x, d))
            end
        end

      open R

    end (* local *)
  end (* struct *)

functor Polynomial_DivMod (
  X : sig
    structure R : RING_DIV
    structure S : AVS_1
    structure I : INTEGER
    val dimension : I.int option
    sharing type S.I.int = I.int
    sharing type S.r = R.t
  end
) =
  struct
    structure P =
      Extend_Polynomial_1 ( X )
  local
    fun divmod (x, y) =
      let open P
        val ord = X.S.foldli
          (fn (i, _, k) =>
            X.I.max (i, k)) index0
        val ox = ord x
        and oy = ord y
      in if X.I.< (ox, oy)
        then (zero, x)
        else let
          val sx = X.S.check x ox
          and sy = X.S.check y oy
          val a = (monomial (X.I.- (ox, oy))) *
            (scalar (X.R./ (sx, sy)))
          val (d', m') = divmod (x - a * y, y)
        in (a + d', m') end
      end
  in
    structure RDM =
      Extend_Ring_DivMod (
        open P

        val op div = #1 o divmod
        val op mod = #2 o divmod
      )
  end
  end

functor Extend_Polynomial_DivMod (
  X : sig
    structure R : RING_DIVMOD
    structure S : AVS_DIVMOD
    structure I : INTEGER
    val dimension : I.int option
    sharing type S.I.int = I.int
    sharing type S.r = R.t
  end
) =
  struct
    local
      structure P =
        Extend_Polynomial_1 ( X )
      structure AR =
        Analytic_DivMod (
          structure R = X.R
          structure I = X.I
        )
      structure AP =
        Analytic_1 (
          structure R = P
          structure I = X.I
        )

      structure IA =
        Index_Analytic (
          structure I = X.I
        )
    in open P

      val extract = fn p =>
        let
          val (dm, x) = extract p
          val (ds, y) = X.S.extract_scalar x
          val none = (NONE, p)
        in
          case dm of NONE => none | SOME m =>
            case ds of NONE => none | SOME a =>
              (SOME ((scalar a) * m), y)
        end

      val derivative =
        let fun der i = AP.iscale
          (monomial (AR.AQR.dec i)) i
        in X.S.comb der
        end

      fun compose p q =
        X.S.comb (AP.ipower q) p

      fun evaluate p c =
        X.S.check (compose p (scalar c))
          AR.AQR.index0

      fun about p c =
        let
          val fac = IA.factorial
          fun f p i = if is_zero p then zero else
            let
              val next = f (derivative p) (AR.AQR.inc i)
              and coef = AR.iscale_ (evaluate p c) (fac i)
            in next + (scalar coef) * (monomial i)
            end
        in f p AR.AQR.index0
        end

    end
  end
