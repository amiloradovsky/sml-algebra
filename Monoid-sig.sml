signature MONOID =
  sig
    type t

    val direct : t * t -> t
  end

signature QUASIGROUP =
  sig
    include MONOID

    val inverse0 : t * t -> t
    val inverse1 : t * t -> t
  end

signature GROUP =
  sig
    include QUASIGROUP
    (* which is now associative *)

    val neutral : t
    val inverse : t -> t
  end

signature HALFGROUP_1 =
  sig
    include MONOID

    val neutral : t
  end
