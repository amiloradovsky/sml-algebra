(*
 * Various integral complex numbers
 *)
 
let open Complex_H
  val x = S.*
  val show = IntInf.toString

  fun show_complex x =
    let
      val re = re x
      and im = im x
      val izre = II_Ring.is_zero re
      val izim = II_Ring.is_zero im
      val ieim = II_Ring.is_zero
        (II_Ring.- (im, II_Ring.one))
      val imim = II_Ring.is_zero
        (II_Ring.+ (im, II_Ring.one))
    in
      if izim then show re else (
        (if izre then "" else (show re) ^ " + ") ^
        (if ieim then "" else
         if imim then "~" else (show im)) ^ "i"
      )
    end
  fun show_list nil = "[]"
    | show_list (x :: xx) =
    let
      fun show_rest nil = ""
        | show_rest (x :: xx) =
          (", " ^ x ^ (show_rest xx))
    in "[ " ^ x ^ (show_rest xx) ^ " ]"
    end
  fun print_line x = print (x ^ "\n")
  val print_complex_list = print_line o
    show_list o (List.map show_complex)
in

let open Complex_H
in print_complex_list [z, e,
    i,
    i * i,
    i * i * i,
    i * i * i * i]
end ;

let open Complex_E
in print_complex_list [z, e,
    i,
    i * i,
    i * i * i,
    i * i * i * i]
end ;

let open Complex_P
in print_complex_list [z, e,
    i,
    i * i,
    i * i * i,
    i * i * i * i]
end ;

(* factorization of 5 and 13 in integral complex *)
let open Complex_H
  val a5  = (x (2, e)) + (x (1, i))
  val a5' = (x (1, e)) + (x (2, i))
  val a13 = (x (2, e)) + (x (3, i))
in print_complex_list [
  abs2 a5 , tr a5 ,
  abs2 a5', tr a5',
  abs2 a13, tr a13]
end

end (* local *)
;

(*
 * Division with remainder
 *)

let open II_Ring
  val x = (12, 15)    (* 4, 5 *)
in [(gcd x, lcd x),    (* 3, 60 *)
  gcdpq2 x, gcdpq1 x, gcdpq0 x]
end ;

let open II_Ring
  val x = (337, 256)
in [(gcd x, lcd x),  (* 1, * *)
  gcdpq2 x, gcdpq1 x, gcdpq0 x]
  (* q_n =    1, 3,  6,   4,   3
   * P_n = 1, 1, 4, 25, 104, 337
   *)
end ;

let open II_Ring
  val to   = IntInf.toInt
  val from = IntInf.fromInt
  fun tab (f, n) = List.tabulate
    (to n, f o from)
  fun list x = case (linear x)
   of SOME fnn => tab fnn
    | NONE    => []
in map list [
  (256, 179, 337),
  (1215, 560, 2755)]
end ;

let open II_UAVS open II_UAVS_DM
  fun inc i = IntInf.+ (i, 1)
  fun fromList x = #2 (List.foldl
    (fn (x, (i, y)) =>
      (inc i, y + singleton (i, x)) )
    (0, zero) x)
  val mm  = fromList [13, 17]
  and bb1 = fromList [ 1, 12]  (* 131 (mod 221) *)
  and bb2 = fromList [ 6,  8]  (* 110 (mod 221) *)
  and bb3 = fromList [11,  4]  (*  89 (mod 221) *)
in List.map (system mm) [bb1, bb2, bb3]
end ;

let open II_UAVS_1 open II_UAVS_DM
  fun inc i = IntInf.+ (i, 1)
  fun fromList x = #2 (List.foldl
    (fn (x, (i, y)) =>
      (inc i, y + singleton (i, x)) )
    (0, zero) x)
  val mm = fromList [25, 27, 59]
in List.map (system mm)
  [ort 0, ort 1, ort 2]
end ;

(*
 * Field (Ring) of partials
 *)

let open II_Partial
  fun rat (a, b) =
    (fromRing (IntInf.fromInt a)) /
    (fromRing (IntInf.fromInt b))
  val x = rat (2, 3)  (* 10:15 *)
  and y = rat (3, 5)  (*  9:15 *)
in map (toRing o reduce) [
  x + y, x - y,  (* 19:15, 1:15 *)
  x * y, y * x,  (* 2:5 *)
  x / y, y / x]  (* 10:9, 9:10 *)
end ;

let open II_Partial
  val a = (fromRing 35) / (fromRing 15)
  val b = (fromRing 35) / (fromRing  7)
in map entire [a, b]  (* 2 + 1:3, 5 *)
end ;

(*
 * Euclidean space
 *)

let open E2_over_II
  local open S
  in
    val o0 = ort Index2.i0
    val o1 = ort Index2.i1
    val a0 = 5 * o0 + 7 * o1
    val a1 = 7 * o0 - 5 * o1
  end
  open E
in II_Ring.is_zero (a0 * a1)
end ;

(*
 * Universal AVS
 *)

let open II_UAVS
  val one = II_Ring.one
  val two = II_Ring.+ (one, one)
  val x = two * (
    singleton (IntInf.fromInt 0, one) +
    singleton (IntInf.fromInt 1, two) +
    singleton (IntInf.fromInt 2, one)
  )
(*
  val x = 3 * (singleton (1, 1) + singleton (2, 4))
  val y = 2 * (singleton (2, 2) + singleton (3, 9))
  val z =      singleton (3, 3) + singleton (4, 16)
*)
in foldri (fn (_, x, y) => II_Ring.+ (x, y)) II_Ring.zero x
end ;

(*
 * Polynomials
 *)

let open II_PPP open II_Poly
  val x = symbol
in

let
  val pp = [
    (one + (scalar 2) * x + x * x),
    ((one + x) * (one + x)),
    ((one + x) * (one - x)),
    ((x - one) * (x * x + x + one))
  ]
in print_poly_list pp
end ;

let
  val pp = [
    (one + (scalar 2) * x + x * x),
    ((one + x) * (one + x)),
    ((one + x) * (one - x)),
    ((x - one) * (x * x + x + one))
  ]
in (print_poly_list o (map derivative)) pp
end ;

(*  2x + 2x^2 = 2x (1 + x)  *)
let
  val (po, q) = extract
    (x + x * x  +  x + x * x)
in case po
 of NONE   => print "FAIL!\n"
  | SOME p => print_poly_list [p, q]
end ;

(*
 * P = (x + 2)(x + 3) = 6 + 5x + x^2
 * P' = 5 + 2x
 * P'' = 2
 *    42 + 13 (x - 4) + (x - 4)^2 =
 *  = 42 + 13x - 52 + x^2 - 8x + 16 = P
 *)
let
  val x1 = 4
  val pol0 = (x + (scalar 2)) * (x + (scalar 3))
  val pol1 = about pol0 x1
  val pol2 = compose pol1 (x - (scalar x1))
  val succ = is_zero (pol2 - pol0)
in print_poly_list [pol1, pol2]; succ
end

end ;

(*  p = x^2 + 5x + 6
 *  q = x^2 +  x - 2
 *  p/q = 1 + (4x + 8) / (x^2 +  x - 2)
 *  r = 4 (x + 2)
 *  q/r = (x^2 +  x - 2) / (4x + 8) =
 *      = 1 / 4 x + (-x - 2) / (4x + 8) =
 *      = 1 / 4 x - 1 / 4 =
 *)
let open IP_PPP
  open IP_Poly
  open P
  val scalar = scalar o II_Partial.fromRing
  val x = symbol
  val p = (x + (scalar 2)) * (x + (scalar 3))
  and q = (x - (scalar 1)) * (x + (scalar 2))
  and r = (scalar 4) * x + (scalar 8)
in print_poly_list [ p, q, r ];
  print_poly_list [ RDM.div (p, q), RDM.mod (p, q) ];
  print_poly_list [ RDM.div (r, q), RDM.mod (r, q) ];
  print_poly_list [ RDM.gcd (p, q), RDM.gcd (q, p) ];
  is_zero (r + q - p)
end ;

(*
 * Analytic summa and product
 *)

let open II_Analytic
  val index3 = fromIndex 3
in map P.toRing [
  integer_scale index3  5,
  integer_power index3  5,
  integer_scale index3 ~5,
  integer_power index3 ~5]
end ;

II_Analytic.factorialsk 1 7;
II_Analytic.factorialsk 2 7;
II_Analytic.binomials 4;
II_Analytic.count_of_monomials_test 37;

(*
 * Modulo
 *)

let open Mod7
	val fi = fromRing o Integer.fromInt
	val ti = Integer.toInt o toRing
  val a = fi 3
  and b = fi 5
in map ti [
  a + b, b + a,
  a - b, b - a,
  a * b, b * a]
end ;
