structure Integer = IntInf
structure II_Ring = Integer_to_Ring_DivMod ( Integer )

(*
 * Various integral complex numbers
 *)

local
  structure II_Complex = Complex ( II_Ring )
in
  structure Complex_H = II_Complex.Hyperbolic
  structure Complex_E = II_Complex.Elliptic
  structure Complex_P = II_Complex.Parabolic
end

structure II_Partial =
  Ring_DivMod_to_Partial (
    structure R = II_Ring
    val commuting = true
  )
;
structure E2_over_II =
  Euclidean_Space (
    structure R = II_Ring
    structure I = Index2
  )
;

structure Integer_Index =
  struct
    type int = Integer.int

    fun same xy = case Integer.compare xy
      of EQUAL => true | _ => false
  end

structure II_UAVS =
  Ring_to_Universal_AVS (
    structure R = II_Ring
    structure I = Integer_Index
  )
;
structure II_UAVS_1 =
  Build_AVS_1 (
    structure R = II_Ring
    structure S = II_UAVS
  )
;
structure II_UAVS_DM =
  Extend_AVS_DivMod (
    structure R = II_Ring
    structure S = II_UAVS_1
  )
;
structure II_Poly =
  Extend_Polynomial_DivMod (
    structure R = II_Ring
    structure S = II_UAVS_DM
    structure I = Integer
    val dimension = NONE
  )
;

structure IP_UAVS =
  Ring_to_Universal_AVS (
    structure R = II_Partial
    structure I = Integer_Index
  )
;
structure IP_UAVS_1 =
  Build_AVS_1 (
    structure R = II_Partial
    structure S = IP_UAVS
  )
;
structure IP_Poly =
  Polynomial_DivMod (
    structure R = II_Partial
    structure S = IP_UAVS_1
    structure I = Integer
    val dimension = NONE
  )
;

structure II_Analytic =
  Index_Analytic (
    structure I = Integer
  )
;
structure Mod7 =
  Modulo (
		structure R = II_Ring
    val modulo = Integer.fromInt 7
  )
;

structure II_PPP =
  PPP (
    structure S = II_UAVS
    structure I = Integer
    val show_ring = Integer.toString
  );
structure IP_PPP =
  PPP (
    structure S = IP_UAVS
    structure I = Integer
    val show_int = Integer.toString
    val show_ring = fn x =>
      let open II_Partial
        val (p, q) = toRing x
        val e = is_zero (#2 (entire x))
      in (show_int p) ^ (if e then ""
        else "/" ^ (show_int q))
      end
  );
