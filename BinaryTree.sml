
structure BinTree =
  struct

  (* TODO: функция балансировки дерева
   *       множества, справочники, удаление
   *)

  datatype 'a tree = Empty | Tree of {
    value : 'a, next : 'a tree * 'a tree }

  fun extract _ Empty _ = nil | extract r ( Tree t ) x =
    let val xt = #value t and ( n0, n1 ) = #next t in
      if r ( xt, x ) then let val found = extract r n1 x in
          if r ( x, xt ) then xt :: found else found end
      else extract r n0 x end

  fun node x tt = Tree { value = x, next = tt }
  fun create x = node x ( Empty, Empty )

  fun insert_tree _ ( Empty, t ) = t
    | insert_tree r ( t0, t1 ) =
    let val Tree { value = x0, next = ( n0, n1 ) } = t0
        and Tree { value = x1, ...               } = t1 in
      if r ( x0, x1 ) then node x0 ( n0, insert_tree r ( n1, t1 ) )
                      else node x0 ( insert_tree r ( n0, t1 ), n1 ) end

  fun insert r t x = insert_tree r ( t, create x )

  fun print_spaces 0 = ()
    | print_spaces n = ( print " " ; print_spaces ( n - 1 ) )

  fun print_tree _ _ Empty = ()
    | print_tree f ( h, dh ) ( Tree t ) =
      let val h' = h + dh and ( n0, n1 ) = #next t in
        print_tree f ( h', dh ) n0 ;
        print_spaces h ;
        print ( f ( #value t ) );
        print "\n" ;
        print_tree f ( h', dh ) n1 end

  end

structure BinExpr =
  struct

  datatype 'a expr = Leaf of 'a | Tree of 'a expr * 'a expr

  fun map_pair f ( x0, x1 ) = ( f x0, f x1 )
  fun evaluate _ ( Leaf x ) = x
    | evaluate f ( Tree tt ) = f ( map_pair ( evaluate f ) tt )

  end

(* tests
  let open BinExpr val e =
    Tree ( Leaf 7, Tree ( Tree ( Leaf 2, Leaf 3 ), Leaf 5 ) )
    in evaluate op + e end ;
*)
;
  let open BinTree
    fun insert_list _ t nil = t
      | insert_list r t ( l :: ll ) = insert_list r ( insert r t l ) ll
    val r = op <=
    val t = insert_list r Empty [ 11, 3, 7, 13, 5, 17, 1, 2 ]
    in  print_tree Int.toString ( 1, 4 ) t ;
        map ( extract r t ) [ 1, 2, 3, 4, 5, 6, 7 ] end ;
