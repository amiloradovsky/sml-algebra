(* Base ring supposed to be commuting *)
functor Polynomial_1 (
  X : sig
    structure S : AVS_1
    structure I : RING_1
    sharing type S.I.int = I.t
  end
) : RING_1 =
  AVS_to_Algebra_1 (
    structure S = X.S

    val one = S.ort I.one
    val table = ( fn ii => S.ort ( I.+ ii ) )
  )

(* integer order function
  val ord = X.S.foldli
    (fn (i, _, k) =>
      X.I.max (i, k)) index0
*)

functor Polynomial_DivMod (
  X : sig
    structure R : RING_DIV
    structure S : AVS_1
    structure I : RING_1
    structure O : ORDERED
    sharing type S.I.int = I.t
    sharing type S.r = R.t

    val ord : S.t -> O.t
  end
) : RING_DIVMOD =
  Extend_Ring_DivMod (

    local open X.S in
      val monomial = ort
      fun monomial_s (i, x) = x * monomial i
      fun scalar x = monomial_s ( X.I.zero, x )
      end

    structure P =
      Polynomial_1 ( X )

    open P

(* TODO: sustraction of orders *)
    fun divmod (x, y) =
      let val ox = X.ord x
          and oy = X.ord y
      in if X.O.>= (ox, oy) then
        let val sx = X.S.check x ox
            and sy = X.S.check y oy
            val a = (monomial (X.I.- (ox, oy))) *
              (scalar (X.R./ (sx, sy)))
            val (d', m') = divmod (x - a * y, y)
        in (a + d', m') end
        else (zero, x)
      end

    val op div = #1 o divmod
    val op mod = #2 o divmod
  )

functor Extend_Polynomial_DivMod (
  X : sig
    structure R : RING_DIVMOD
    structure S : AVS_DIVMOD
    structure I : INTEGER
    val dimension : I.int option
    sharing type S.I.int = I.int
    sharing type S.r = R.t
  end
) =
  struct
    local
      structure P =
        Extend_Polynomial_1 ( X )
      structure AR =
        Analytic_DivMod (
          structure R = X.R
          structure I = X.I
        )
      structure AP =
        Analytic_1 (
          structure R = P
          structure I = X.I
        )

      structure IA =
        Index_Analytic (
          structure I = X.I
        )
    in open P

      val derivative =
        let fun der i = AP.iscale
          (monomial (AR.AQR.dec i)) i
        in X.S.comb der
        end

      fun compose p q =
        X.S.comb (AP.ipower q) p

      fun evaluate p c =
        X.S.check (compose p (scalar c))
          AR.AQR.index0

      fun about p c =
        let
          val fac = IA.factorial
          fun f p i = if is_zero p then zero else
            let
              val next = f (derivative p) (AR.AQR.inc i)
              and coef = AR.iscale_ (evaluate p c) (fac i)
            in next + (scalar coef) * (monomial i)
            end
        in f p AR.AQR.index0
        end

    end
  end
