
 (* TODO:
  * использовать собственное отношение эквивалентности для вершин
  * рассчёт электрических цепей постоянного тока
  * системы линейных уравнений и симплекс-метод
  *)

signature GRAPH =
  sig

  type ( ''v, 'e ) graph

  val paths : ( ''a, 'b ) graph -> ''a list -> ( ''a * ''a ) -> ''a list list
  val from_list : ( ''a * ''a ) list -> ( ''a, bool ) graph

  end

structure Graph (* :> GRAPH *) =
  struct

  datatype ( ''v, 'e ) graph = Graph of { from : ''v -> ''v list, value : ''v * ''v -> 'e }

  fun isin l e = case List.find ( fn x => x = e ) l
    of NONE => false | SOME _ => true

  fun paths ( Graph g ) s0 ( v0, v1 ) =
    if isin s0 v0 then nil else
    if v0 = v1 then ( v0 :: nil ) :: nil else
      ( print " "; print ( Int.toString v0 );
      let val f = fn v => map ( fn p => v0 :: p )
        ( paths ( Graph g ) ( v0 :: s0 ) ( v, v1 ) )
      in foldl ( fn ( x, s ) => x @ s ) nil
        ( map f ( #from g v0 ) ) end
      )

  fun from_list edges =
    let val f = fn v => map ( fn ( _, v1 ) => v1 )
          ( List.filter ( fn ( v0, _ ) => v = v0 ) edges )
        and e = fn vv => case List.find ( fn vv' => vv = vv' ) edges
          of NONE => false | SOME _ => true
    in Graph { from = f, value = e } end

  end
;
  let open Graph val g = from_list
    [ ( 1, 2 ), ( 2, 3 ), ( 3, 4 ), ( 4, 1 ), ( 2, 4 ), ( 4, 3 ) ]
  in print "Paths:\n" ; paths g nil ( 1, 3 ) end ;
