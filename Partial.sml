functor Ring_to_Partial (
    structure R : RING_1
    val commuting : bool  (* or anticommuting *)
) :> RING_PARTIAL where type r = R.t =
  struct
    structure R = R

    type r = R.t
    type t = r * r

    fun fromRing x = (x, R.one)
    fun   toRing x = x

(*
    if [y0, y1] = 0  (y0 and y1 is commuting)
    or (y0, y1) = 0  (is anti-commuting)
    then
              x0 y0' + x1 y1' =
     = x0 y1 y1' y0' + x1 y0 y0' y1' =
    = x0 y1 (y0 y1)' + x1 y0 (y1 y0)' =
    = (x0 y1 + x1 y0) (y0 y1)'  (commuting)
 or = (x0 y1 - x1 y0) (y0 y1)'  (anti-commuting)
*)
    fun op + ((x0, y0), (x1, y1)) =
      let open R
        val sum = (if commuting
          then op + else op -)
      in (sum (x0 * y1, x1 * y0), y0 * y1)
      end
    fun op ~ (x, y) = (R.~ x, y)
    fun op - (x, y) = x + (~y)
    val zero = fromRing R.zero
    fun is_zero (x, _) = (R.is_zero x)

    fun op * ((x0, y0), (x1, y1)) =
      let open R
      in (x0 * x1, y0 * y1)
      end
    fun rev (x, y) = if (R.is_zero y)
      then raise Div else (y, x)
    fun op / (x, y) = x * (rev y)
    val one = fromRing R.one
  end

functor Ring_DivMod_to_Partial (
  X : sig
    structure R : RING_DIVMOD
    val commuting : bool
  end
) :> RING_PARTIAL_DM where type r = X.R.t =
  struct

    structure R = X.R
    structure P = Ring_to_Partial ( X )

    open P

    fun reduce a =
      let
        val (xy as (x, y)) = toRing a
        val g = R.gcd xy
      in (fromRing (R.div (x, g))) /
         (fromRing (R.div (y, g)))
      end

    fun entire a =
      let open R
        val (x, y) = P.toRing a
        val d = x div y
        and m = x mod y
      in (d, (fromRing m) / (fromRing d))
      end

  end
